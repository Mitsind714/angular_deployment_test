import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from '../popup/popup.component';
import { MatSort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import { ApiService } from '../general/api.service';
import { GlobalFunctionService } from '../../utilities/services/global-function.service';
//import {FunctionsService} from '../general/';
import {TooltipPosition} from '@angular/material/tooltip';
export interface programs {
  agencyName:string,
      programName:string,
      contactPerson:string,
      city:string,
      state:string,
      emailAddress:string,
      status:string
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  position = new FormControl("below");


  displayedColumns: string[] = ['agencyName', 'programName','contactPerson', 'city', 'state','emailAddress', 'status', "actions"];
  // filterColumns:Array<string>=['programNameFilter','orgNameFilter','createdAtFilter','statusFilter','actionsFilter'];

  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private apiService: ApiService, public dialog: MatDialog, public globalFunctionService: GlobalFunctionService) { }

  ngOnInit(): void {
    this.list();

  }

  openDialog(action, record) {
    if (action != "edit") {
      let dialogRef = this.dialog.open(PopupComponent, {
        width: '640px', disableClose: true, data: { action: action, record: record }
      })
      dialogRef.afterClosed().subscribe(result => {
        this.ngOnInit();
        console.log(`Dialog Result:${result}`)
      })
    }else{
      console.log("Clicked Edit icon");
      if((record.status == "Active") ||(record.status == "Inactive")){
        console.log("Status is Active or inactive");

         let dialogRef = this.dialog.open(PopupComponent, {
          width: '640px', disableClose: true, data: { action: action, record: record }
        })
        dialogRef.afterClosed().subscribe(result => {
          this.ngOnInit();
          console.log(`Dialog Result:${result}`)
        })
      }else{
        console.log("Status is not active or inactive");
         this.globalFunctionService.openSnackBar(
          'INFO',
          { message: "Program cannot be modified", icon: 'info', position: 'before' },
          { verticalPosition: 'top',panelClass:  ['snack-bar-color']  }
        );
      }
    }
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  async list() {


    try{
      console.log("List method Called")
      const apiData: any = {};
      const headers: any = {};
      const response =  this.apiService.list();
      console.log(response);
      // if(response.state === 'SUCCESS' && response.result.record.length){
        const filteredPrograms=response;
        console.log(filteredPrograms);
        //this.functionService.arrayAlter(filteredPrograms);
        this.dataSource = new MatTableDataSource<programs>(filteredPrograms);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      // }else{
      //   console.log(" In Else block")
      // }
     
    } catch(error){
      if (error.hasOwnProperty('error') && error.error.hasOwnProperty('message')) {
        this.globalFunctionService.openSnackBar(
          'ERROR',
          { message: error.error.message, icon: 'error', position: 'before' },
          { verticalPosition: 'top',panelClass:  ['snack-bar-color'] }
        );
      }
    }finally{

    }
   
  }

}
