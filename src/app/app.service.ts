import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http:HttpClient) { }
  getJsonData(){
    let URL="/assets/json/opt_accred_docs.json";
    return this.http.get(URL);
  }
}
