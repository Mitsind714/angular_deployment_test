import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortner'
})
export class ShortnerPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    if(value.length>18){
    return value.substring(0,18)+"...";
    }else{
      return value;
    }
  }
}