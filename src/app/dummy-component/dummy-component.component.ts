import { Component, OnInit, ViewChild } from '@angular/core';
import {  AppService } from '../app.service';
declare var $: any;


@Component({
  selector: 'app-dummy-component',
  templateUrl: './dummy-component.component.html',
  styleUrls: ['./dummy-component.component.css']
})
export class DummyComponentComponent implements OnInit {

  constructor(private service:AppService) { }
  
  ngOnInit(): void {
    this.getJsonData();
    
 
  }

  displayedColumns= ['serialno', 'doctype','description', 'documentname','datecreated', 'action'];
  // jsonData=[
  //   {
  //     serialno:1,
     
  //     description:'',
      
  //   },
  //   {
  //     serialno:2,
  //     description:'',
  //     documentsname:'Empty',
  //   },
  //   {
  //   serialno:3,
  //     description:'',
  //     documentsname:'Empty'
  // } ,
  // {
  //   serialno:4,
  //     description:'',
  //     documentsname:'Empty'
  // }   
  // ]
  dataSource ;
  responseData=null;
  errorDisplay='none';
  getJsonData(){
    console.log("It is called::::!");
    this.service.getJsonData().subscribe((data)=>{
      this.dataSource=data['documents'];
       console.log("dataSource data:::::",this.dataSource);
    //   console.log("this.jsonData.length:::::",this.jsonData.length);
    //  // this.dataSource=data;
    //  let i;
    //  for(i=0;i<this.jsonData.length;i++){
    //   console.log(":::::::::::::", this.jsonData[i],this.responseData.documents);
    //  this.jsonData[i].description= this.responseData.documents[i].Description;
    // }
    // console.log("this.jsonData:::",this.jsonData);
    // this.dataSource=this.jsonData;
    })
    
  
    
  
    
  }
  // upload(event){
  // console.log(":::::::",event);
  // }
  // uploadApp(event){
  //   console.log(":::::::",event);

  // }
 
 @ViewChild("upload") file;
  uploadlink(index){
    this.index=index;

    //console.log("element::::::",element);
    this.file.nativeElement.click(); 
  }
  index;

  onFileUpload(event){
//console.log("index:::::",index);
// console.log("fileEvent:::::",fileEvent.target.files[0].name);
 this.dataSource[this.index].Documentsname=event.target.files[0].name;
 this.dataSource[this.index].DateCreated=new Date();
 if(this.dataSource[this.index].Required){

 }

}
  remove(index){
    this.index=index;
    this.dataSource[this.index].Documentsname=null
    this.dataSource[this.index].DateCreated=null;

  }

  submit(){
    for(let i=0;i<this.dataSource.length;i++){
      if(this.dataSource[i].Required){
        if(!this.dataSource [i].Documentsname){
          this.errorDisplay='block';
        }else{
          this.errorDisplay='none';

        }
       
      }
    }
  }
  cancel(){
    for(let i=0;i<this.dataSource.length;i++){
      this.dataSource[i].Documentsname=null
      this.dataSource[i].DateCreated=null;
      this.errorDisplay='none';

    }
  }
}

